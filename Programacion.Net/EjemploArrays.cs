using System;

//es una estruc de datos que puede tener 0 o muchos elem del mismo tipo.
//El nº de elementos  es fijo.
public class EjemploArrays
{
	public static void Main () 
		{
			//ArrayDeDatosPrimitivos();
			//ArrayDeFloats();
			ArrayDeStrings();
		}
	public static void ArrayDeDatosPrimitivos() 
	{
		Console.WriteLine( "Array enteros");
		// declaracion del tipo con el nombre de la variable.
		int[]numerosPares;
		// iniciar con el nº de elementos.
		numerosPares=new int[4];
		// new se usa para reservar la memoria para cada estructura.
		// Para aasignar un valor en la variable se usan los corchetes.
		// Los arrays empiezan por 0
		numerosPares[0]= 2;
		numerosPares[1]= 4;
		numerosPares[2]= 6;
		numerosPares[3]= 8;
		// podemos pensar q es como una columna de Excel
		
		for(int i = 0; i< numerosPares.Length; i=i+1)
		{
		Console.WriteLine(" Elemento"+ i +"="+numerosPares[i]);
		}
	}
		
	public static void ArrayDeFloats()
	{
		float[] fuerzas = new float[5];
		int i = 0;
		for(; i <= fuerzas.Length - 1;i += 1)
		{
			fuerzas[i]=1.2345f + i;
		}
		Console.WriteLine( "que fuerza quieres ver");
		string strTecla = Console.ReadKey().Key.ToString();
		string numero;
		//Para extraer el nº, probamos varias maneras.
		//1. usando string.Remove(). quitamos lo q no queremos.
		numero = strTecla.Remove(0,strTecla.Length-1);
		// 2. usando el string como si fuera un array bidimensional de char
		numero= strTecla[strTecla.Length-1].ToString();
		// 3. usando la tipica para estos casos q es substring,lo contrario de Remove
		numero= strTecla.Substring(strTecla.Length-1,1);
		int pos= Int32.Parse (numero);
		Console.WriteLine( "La fuerza nº"+ pos +"es de:");
		Console.WriteLine( fuerzas[pos]+ "newtons");
	}
	public static void ArrayDeStrings()
	{
		Console.WriteLine( "cuantos nombres quiere guardar");
		string strCantidad= Console.ReadLine();
		int cantidad= Int32.Parse(strCantidad);
		string[]nombres;
		nombres=new string[cantidad];	
		for (int i = 0; i< cantidad; i++)
		{
			string nuevoNombre= Console.ReadLine();
			nombres[i] = nuevoNombre;	
		}
		Console.WriteLine( "listado de nombres");
		
		for(int i = 0; i< cantidad; i++)
		{
			Console.WriteLine($"-{i}-{nombres[i]}");
			//Console.WriteLine(" "-{i}-{nombres[i]}");
		}
	//crear una funcion que calcule la suma de los ataques cercanos.
	/*Arrays ataques:   
						3,2 False
						1,7 True
						7,4 True
						5,0 False
						7,1 True
						4,8 True*/
						
	// se consideran ataques cercanos los true. 
	
		float[]ataques={1.7f,2.4f, 7.1f,4.8f};
		ataques[0]= 3.2f;
		ataques[1]= 1.7f;
		ataques[2]= 7.1f;
		ataques[3]= 4.8f;
		
		for(int i = 0; i< ataques.Length; i=i+1)
		{
		Console.WriteLine(" Ataques"+ i +"="+True[i]);
		}			
						
						
						
						
						
	}

}