 using System;
 
 /* Estos son los tipos primitivos de c# (
 igual que en otros lenguajes de programacion)
 */
 
 
 
public class TiposDeDatos 
{
    public static void Main () 
	{
		byte unByte = 255;
		char unCaracter= 'A';
		
		
		int numEntero= 1000;//no entre -2000.000.000 y 2000.000.000 porque ocupa 4 bytes
		
		Console.WriteLine (" Byte"+unByte +"char" +unCaracter );
		Console.WriteLine (" un char ocupa" + sizeof (char)+ "bytes" );
		Console.WriteLine (" El entero vale" +numEntero );
		Console.WriteLine (" Y ocupa" + sizeof(int) + " bytes" );
		numEntero = 2000000000;
		Console.WriteLine (" Ahora El entero vale" + numEntero );
		// tipos decimales lfoat de 4 bytes y double de 8 bytes
		//precision es de 7-8 cifras 15-16 cifras en total.
		long enteroLargo = 500000000000;
		Console.WriteLine ("Entero Largo vale" + enteroLargo);
		float numDecimal = 123456789f;
	
		Console.WriteLine (" Num decimal Float vale" +numDecimal );
		//para mas precision Double
		double numDoblePrecicion = 12345.67890123456789;
		Console.WriteLine (" Num decimal doble vale" +numDoblePrecicion );
		
		//Para guardar si o no verdad o falso, 0,1.
		bool variableBooleana= true;
		Console.WriteLine (" variable Booleana vale" +variableBooleana );
		//podemos guardar coparaciones, condiciones, etc
		variableBooleana= numDecimal >1000;
		Console.WriteLine (" variable Booleana ahora vale" +variableBooleana );
		
		string cadenaDeTexto= " una cadena de datos";
		Console.WriteLine (cadenaDeTexto );
		Console.WriteLine (cadenaDeTexto +"que"+"permite concatenacion" );
		//Lo que no permite son conversiones invalidas:
		int otroNumero=5.234f;
		bool otroNumero=1;
		string otrotext=1;
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
 }