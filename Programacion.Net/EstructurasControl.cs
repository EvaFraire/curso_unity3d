
using System;
public class EstructurasControl
{
	public static void Main()
	{
	/*EjemploIfSimple();
	EjemploIfComplicado();
	EjemploIfSumas();
	EjemploIfConsecutivos();*/
	EjemploBucleWhile();
	}
	static void EjemploIfSimple()
	{
		//Condicional simple :if( booleano)instruccion,
		if(true) Console.WriteLine ("Pues si");
		if(false) Console.WriteLine("Pues va a ser que no");
		bool oSioNO=true;
		if (oSioNO)Console. WriteLine ("Pues tambien asi");
		//o recibimos condicionales
		if(5==5)Console. WriteLine ("Pues 5=5");
		if(4>7)Console. WriteLine ("Puespues esto tampoco se muestra");
		
	}
	static void EjemploIfComplicado()
	{
		// if complicado. if(booleano) instruccion de cuando es verdad
		//;else instruccion que es mentira
		if(4>= 7)Console. WriteLine ("4>= 7");
		else Console. WriteLine ("4< 7");
		// podemos separar en varias lineas
		if("Hola"!= "hola")Console. WriteLine ("son dist");
		else Console. WriteLine ("Son =");
		
	}
	static void EjemploIfSumas()
	{
		//Ejercicio:
		string numA="20", numB="30",numC="40";
		int resultado=60;
		//suma las tres combinaciones (A+B, B+C, A+C) y que el programa 
		//diga cual es igual al resultado.
		//la consola debe mostrar:
		//mostrar los valores A=20, B=30; C=40, resultado=50
		//A+B es igual a resiultado
		//A+C es distinto del resultado
		//B+C es distinto del resultado
		
		/*int intNumA= Int32.Parse(numA);
		int intNumB= Int32.Parse(numB);
		int intNumC= Int32.Parse(numC);
		int resultadoSuma= (intNumA+intNumB+intNumC);
		Console.WriteLine ("el resultado  de A+B+C es"+resultadoSuma);*/
		
		int intA, intB, intC;
		 intA= Int32.Parse(numA);
		 intB= Int32.Parse(numB);
		 intC= Int32.Parse(numC);
		if (intA+ intB== resultado)
		Console.WriteLine ("el resultado  de A+B es igual a"+ resultado);
		else
		Console.WriteLine ("el resultado  de A+B es distinto a" +resultado);
		if (intB+ intC== resultado)
		Console.WriteLine ("el resultado  de B+C es igual a"+ resultado);
		else
		Console.WriteLine ("el resultado  de B+C es distinto a"+ resultado);
		if (intA+ intC== resultado)
		Console.WriteLine ("el resultado  de A+C es igual a" +resultado);
		else
		Console.WriteLine ("el resultado  de A+C es distinto a" +resultado);
	}
	static void EjemploIfConsecutivos()
	{
		Console.WriteLine("Introduzca su opcion");
		Console.WriteLine("1 Introduzca su opcion1");
		Console.WriteLine("2 Introduzca su opcion2");
		Console.WriteLine("3 Introduzca su opcion3");
		Console.WriteLine("(*)Cualquier otra  opcion");
		
		ConsoleKeyInfo opcion= Console.ReadKey();
		ConsoleKey conKey= opcion.Key;
		string caracter = conKey.ToString();
		
		Console.WriteLine(">>" + caracter);
		// Si el caracter es un 1 del teclado normal o bien
		//Si el caracter es un numero del teclado numerico
		
		if(caracter == "NumPad1"||caracter == "D1")
		Console.WriteLine("has elegido la opcion 1");
		else if(caracter =="NumPad2"|| caracter =="D2")
		Console.WriteLine("has elegido la opcion 2");
		else if(caracter =="NumPad3"|| caracter =="D3")
		Console.WriteLine("has elegido la opcion 3");
		else 
		Console.WriteLine("opcion no contemplada");
		
	}
	// El bucle while puede crear cualquier tipo de bucle
	// puede que no se ejecute si el boleano es falso de entrada
    //	o q sea ubucle infinito si la condicion es true.
    // o un bucle normal con una duracion determinada
	static void EjemploBucleWhile()
	{
		Console.WriteLine(" Antes de bucle");
		while (false)
		Console.WriteLine(" instruccion que No se repite");	
	    // se necesita un contador  para que se repita y una condicion 
        //	para que se ejecute
	    int contador=0;
	    while (contador<10)
			// se puede sustituir una instruccion por un bloq que 
		//ira entre parentesis
		{
			 Console.WriteLine(" contador" +contador);
			 contador= contador+1;
		}
	    bool siContinuar= true;
	    while(siContinuar)
	    {
		 Console.WriteLine(" estamos en  bucle. Desea salir"); 
		 string tecla= Console.ReadKey().Key.ToString();
		 if (tecla == "S"||tecla =="s")
		 {
			 siContinuar=false;
		 }
	    }
		Console.WriteLine(" fin de bucle");
		//con ctrol C se para la consola.
		
	static void EjemploBucleFor()
	{
		// un bucle For es un bloque while q se suele usar con un contador.
		//estructura:for( inicializacion; condicion; incremento)
		// instruccion;o bien un bloque q se repite.
		for(int contador=0; contador<10;contador = +contador)
		{
			Console.WriteLine(" contador =" + contador);
		}
	}
	static void EjemploBucleDoWhile()
	{	
		// es como un bucle while, pero se ejecuta al menos una vez
		do
		{
		Console.WriteLine(" Al menos una vez" );
		}
		while (false);
		 bool siSalir= true;
	    do
		{
			Console.WriteLine(" hasta q la condicion sea falsa" );
			siSalir=! siSalir;
		}
		while(!siSalir);
		
	}
}
















