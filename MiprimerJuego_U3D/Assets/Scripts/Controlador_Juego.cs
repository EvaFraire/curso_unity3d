﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_Juego : MonoBehaviour

{ public GameObject prefabLata;
    public GameObject prefabLata2;
    public GameObject prefabBotella;
    public int vidas = 7;
    public int puntos = 0;
    public GameObject textoVidas;
    public GameObject textoPuntos;

    private int puntosAnt; // Puntos del frame anterior

    // Start is called before the first frame update
    void Start()
    {// El codigo Instantiate sirve para crear un nuevo objeto, en este caso latas y botellas
     // Aproximadamete la mitad  de probabiliodades.
        this.InstanciarEnemigo();
    }

    // Update is called once per frame
    void Update()
    {
        this.textoVidas.GetComponent<UnityEngine.UI.Text>().text = "Vidas:" + this.vidas;
        this.textoPuntos.GetComponent<UnityEngine.UI.Text>().text = "Puntos:" + this.puntos;
        // Podemos utilizar los puntos como una forma de detectar que hemos acabado con el enemigo y
        //Asi nos puede salir otro enemigo nuevo
        //necesitamos guardar los puntos del frame anterior, para saber si hay modificaciones
        // y asi poder lanzar un nuevo enemigo o no.

        /* if (this.puntos) ! = this. puntosAnt

          this instanciasEnemigo();
         // Con esto anterior podriamos hacer que salga un enemigo mientras tu hayas ganado puntos en el frame anterior 
         //guardarnos los puntos de cada frame, guardamos los puntos anteriores.
          this.puntosAnt = this.puntos;*/


    }
    //Esto es un nuevo metodo ( accion, conjunto de instrucciones, funcion, procedimiento, mensaje,  etc)
    public void CuandoPerdemosEnemigo()
    {
        this.vidas = this.vidas - 1;        // this.vidas -= 1;   this.vidas--;
        this.InstanciarEnemigo();
    }
    public void CuandoCapturamosEnemigo()
    {
        this.puntos = this.puntos +50;
        
        this.InstanciarEnemigo();
    }

    /*public void InstanciarEnemigo()
     {
        this.vidas=this.vidas
        GameObjec.tInstantiate(prefabLata);
     este seria el metodo para crear un enemigo si siguieramos la  orden que tenemos mas arriba*/
    public void InstanciarEnemigo()

    { /*if (Random.Range(0, 3) == 0)
        {
            GameObject.Instantiate(prefabLata);
        }

        else if (Random.Range(0, 3) == 0) 
        {
           GameObject.Instantiate(prefabLata2);
        }
        else 
        {
            GameObject.Instantiate(prefabBotella);
        }
    }*/
        int numEnemigo = Random.Range(0, 3);
        if (numEnemigo == 0)
        {
            GameObject.Instantiate(prefabLata);
        }  
        else if (numEnemigo == 1)
        {
            GameObject.Instantiate(prefabEnemigo_Lata_2);
        }
        else if (numEnemigo == 2)
        {
            GameObject.Instantiate(prefabBotella);
        }

    }
