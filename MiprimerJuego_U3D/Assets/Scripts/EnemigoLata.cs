﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoLata : MonoBehaviour


{
    public float velocidad;//Por defecto, cero
    GameObject jugador;   // fuera la variable jugador para que sea publica
                           //y siempre tenga  acceso a ella
    

    // Start is called before the first frame update
    void Start()
    {
        //Cuando arranque queremos una posicion aleatoria
        float posInicioX = Random.Range(-10, 10);
        //Le decimos cual es la posicion actual de nuestro componente
        this.transform.position = new Vector3(posInicioX, 12, 1);
        //Para que colisionen ambos objetos, lo primero es obtener
        //el scritp del otro personaje, pero antes hay que indicarle
        //el objeto jugador
        //siempre ponemos que busque el GameObjet en void Start, poeque
        //si lo ponemos en update lo ralentizara
        jugador = GameObject.Find("Jugador_Caballito");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movAbajo = velocidad * new Vector3(0, -1, 0) * Time.deltaTime;
        //en este caso no es como si utilizaramosTranslate, porque aqui se va a segui r 
        //desplazando hacia abajo 
        //aunque rotemos la imagen delpersonaje,
        // en cambio, con traslate seva a mover en relacion  a esa rotacion  que añadimos

        this.GetComponent<Transform>().position = this.GetComponent<Transform>().position + movAbajo;

        if (this.GetComponent<Transform>().position.y < 0)

        { 
            this. GetComponent<Transform>().position = new Vector3(this.transform.position.x, 0, 1);
            if (this.transform.position.x >= jugador.transform.position.x - 3.2f / 2
                && this.transform.position.x <= jugador.transform.position.x + 3.2f /2)
            
            {
                Destroy(this.gameObject);
               
            } else
           
            {
                GameObject.Find("Controlador_Juego").GetComponent<Controlador_Juego>().vidas -= 1;

                Destroy(this.gameObject);
               
            }

            if (jugador.transform.position.x-3.2f/2 <= this.transform.position.x 
                   && jugador.transform.position.x-3.2/2 >= this.transform.position.x)
            {
                /*GameObject.Find("Controlador_Juego").GetComponent<Controlador_Juego>().puntos = +50;*/
                GameObject.Find("Controlador_Juego").GetComponent<Controlador_Juego>().CuandoCapturamosEnemigo();
            }








        }

            







    }
}
